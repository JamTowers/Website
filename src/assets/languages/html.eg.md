```html
<!DOCTYPE html>
<html>
  <body>
    <nav>
      <a href="/index">Home Page</a>
      <a href="/first">First Page</a>
      <a href="/second">Second Page</a>
    </nav>
    <main>
      <h1>Title</h1>
      <p>Interesting text</p>
    </main>
    <footer>Copyright 2020 James Towers &copy;</footer>
  </body>
</html>
```
