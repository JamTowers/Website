import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatCardModule } from "@angular/material/card";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatDividerModule } from "@angular/material/divider";
import { ClipboardModule } from "@angular/cdk/clipboard";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MarkdownModule } from "ngx-markdown";

import { ProjectsRoutingModule } from "./projects-routing.module";
import { ProjectsComponent } from "./projects.component";
import { ProjectDetailsComponent } from "./project-details/project-details.component";

@NgModule({
  declarations: [ProjectsComponent, ProjectDetailsComponent],
  imports: [
    CommonModule,
    MatCardModule,
    ProjectsRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule,
    MatProgressBarModule,
    MatSnackBarModule,
    ClipboardModule,
    MarkdownModule.forChild(),
  ],
})
export class ProjectsModule {}
