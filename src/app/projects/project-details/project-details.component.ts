import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Clipboard } from "@angular/cdk/clipboard";
import { MatSnackBar } from "@angular/material/snack-bar";

import { Project } from "../project";
import { ProjectsService } from "../projects.service";
import { TitleService } from "../../title.service";

@Component({
  selector: "jam-project-details",
  templateUrl: "./project-details.component.html",
  styleUrls: ["./project-details.component.scss"],
})
export class ProjectDetailsComponent implements OnInit {
  project: Project;
  pLoading: boolean = true;
  mdLoading: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private projectsService: ProjectsService,
    private titleService: TitleService,
    private clipboard: Clipboard,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    const projects = this.projectsService.getProjects();
    const paramMap = this.route.paramMap;

    projects.subscribe((projects: Project[]) => {
      paramMap.subscribe((params) => {
        this.project = projects.find(
          (project) => project.name == params.get("projectName")
        );

        this.titleService.setTitle(this.project.name);

        this.pLoading = false;
      });
    });
  }

  copyLink() {
    if (this.clipboard.copy(window.location.href)) {
      this.snackBar.open("Copied to clipboard!", null, {
        duration: 5000,
      });
    } else {
      this.snackBar.open(
        "Couldn't copy to clipboard for some reason, try again.",
        null,
        {
          duration: 10000,
        }
      );
    }
  }

  onLoad() {
    this.mdLoading = false;
  }
}
