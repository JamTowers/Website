import { Component } from "@angular/core";

import { TitleService } from "src/app/title.service";

@Component({
  selector: "jam-concepts",
  templateUrl: "./concepts.component.html",
  styleUrls: ["./concepts.component.scss"],
})
export class ConceptsComponent {
  loading: boolean = true;

  constructor(private titleService: TitleService) {
    this.titleService.setTitle("Concepts");
  }
}
