import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MarkdownModule } from "ngx-markdown";

import { ConceptsComponent } from "./concepts.component";

describe("ConceptsComponent", () => {
  let component: ConceptsComponent;
  let fixture: ComponentFixture<ConceptsComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          HttpClientTestingModule,
          MarkdownModule.forRoot({
            loader: HttpClientTestingModule,
          }),
          MatProgressBarModule,
        ],
        declarations: [ConceptsComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ConceptsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
