import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MarkdownModule } from "ngx-markdown";

import { FrameworkComponent } from "./framework.component";
import { SkillHeaderComponent } from "../../skill-header/skill-header.component";

describe("FrameworkComponent", () => {
  let component: FrameworkComponent;
  let fixture: ComponentFixture<FrameworkComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          HttpClientTestingModule,
          MarkdownModule.forRoot({
            loader: HttpClientTestingModule,
          }),
          MatProgressBarModule,
        ],
        declarations: [FrameworkComponent, SkillHeaderComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameworkComponent);
    component = fixture.componentInstance;
    component.framework = {
      name: "Test Framework",
      competency: 1,
      fragment: "test-framework",
    };
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
