import { Component, Input } from "@angular/core";

import { Framework } from "../framework";

@Component({
  selector: "jam-framework",
  templateUrl: "./framework.component.html",
  styleUrls: ["./framework.component.scss"],
})
export class FrameworkComponent {
  @Input() framework: Framework;
  loading: boolean = true;
  exampleLoading: boolean = true;

  constructor() {}
}
