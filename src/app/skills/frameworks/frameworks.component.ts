import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { TitleService } from "src/app/title.service";
import { Framework } from "./framework";
import { SharedSkillsComponent } from "../shared-skill.component";

@Component({
  selector: "jam-frameworks",
  templateUrl: "./frameworks.component.html",
  styleUrls: ["./frameworks.component.scss"],
})
export class FrameworksComponent
  extends SharedSkillsComponent
  implements OnInit
{
  frameworks: Framework[];

  constructor(
    private titleService: TitleService,
    private route: ActivatedRoute
  ) {
    super(route);
    this.titleService.setTitle("Frameworks");
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.route.data.subscribe((data: { frameworks: Framework[] }) => {
      console.log(data);
      this.frameworks = data.frameworks;
    });
  }
}
