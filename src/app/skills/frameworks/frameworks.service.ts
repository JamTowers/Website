import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";

import { Framework } from "./framework";

@Injectable({
  providedIn: "any",
})
export class FrameworksResolverService implements Resolve<Framework[]> {
  frameworksUrl = "assets/frameworks.json";

  constructor(private http: HttpClient) {}

  resolve(): Observable<Framework[]> {
    return this.http.get<Framework[]>(this.frameworksUrl);
  }
}
