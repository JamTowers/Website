import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";

import { FrameworksResolverService } from "./frameworks.service";

describe("FrameworksService", () => {
  let service: FrameworksResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(FrameworksResolverService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
