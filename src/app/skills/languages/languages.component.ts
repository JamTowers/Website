import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { TitleService } from "src/app/title.service";
import { LanguageSections } from "./language";
import { SharedSkillsComponent } from "../shared-skill.component";

@Component({
  selector: "jam-languages",
  templateUrl: "./languages.component.html",
  styleUrls: ["./languages.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LanguagesComponent
  extends SharedSkillsComponent
  implements OnInit
{
  languageSections: LanguageSections;
  get languageSectionsKeys(): string[] {
    return ["Logical languages", "Data/Structure Languages"];
  }

  constructor(
    private titleService: TitleService,
    private route: ActivatedRoute
  ) {
    super(route);
    this.titleService.setTitle("Languages");
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.route.data.subscribe((sections: { languages: LanguageSections }) => {
      this.languageSections = sections.languages;
    });
  }
}
