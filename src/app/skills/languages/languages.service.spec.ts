import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";

import { LanguagesResolverService } from "./languages.service";

describe("LanguagesService", () => {
  let service: LanguagesResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(LanguagesResolverService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
