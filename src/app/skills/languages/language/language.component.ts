import { Component, Input } from "@angular/core";

import { Language } from "../language";

@Component({
  selector: "jam-language",
  templateUrl: "./language.component.html",
  styleUrls: ["./language.component.scss"],
})
export class LanguageComponent {
  @Input() language: Language;
  loading: boolean = true;
  exampleLoading: boolean = true;

  constructor() {}
}
