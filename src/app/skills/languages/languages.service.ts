import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";

import { LanguageSections } from "./language";

@Injectable({
  providedIn: "any",
})
export class LanguagesResolverService implements Resolve<LanguageSections> {
  languagesUrl = "assets/languages.json";

  constructor(private http: HttpClient) {}

  resolve(): Observable<LanguageSections> {
    return this.http.get<LanguageSections>(this.languagesUrl);
  }
}
