import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { SkillsComponent } from "./skills.component";
import { ConceptsComponent } from "./concepts/concepts.component";
import { LanguagesComponent } from "./languages/languages.component";
import { FrameworksComponent } from "./frameworks/frameworks.component";
import { AdditionalComponent } from "./additional/additional.component";
import { LanguagesResolverService } from "./languages/languages.service";
import { FrameworksResolverService } from "./frameworks/frameworks.service";
import { ConstructionWrapperComponent } from "../shared/construction-wrapper/construction-wrapper.component";

const routes: Routes = [
  {
    path: "",
    component: ConstructionWrapperComponent,
    children: [
      { path: "", component: SkillsComponent },
      { path: "concepts", component: ConceptsComponent },
      {
        path: "languages",
        component: LanguagesComponent,
        resolve: {
          languages: LanguagesResolverService,
        },
      },
      {
        path: "frameworks",
        component: FrameworksComponent,
        resolve: {
          frameworks: FrameworksResolverService,
        },
      },
      { path: "additional", component: AdditionalComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SkillsRoutingModule {}
