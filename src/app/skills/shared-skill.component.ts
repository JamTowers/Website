import { Component, OnInit, AfterViewInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
  template: "This component should be inherited.",
})
export abstract class SharedSkillsComponent implements OnInit, AfterViewInit {
  fragment: string;
  private loaded: boolean = false;

  constructor(private _route: ActivatedRoute) {}

  public ngOnInit(): void {
    this._route.fragment.subscribe((anchor) => {
      if (anchor) {
        this.fragment = anchor;
        if (this.loaded) {
          this.scrollTo(anchor);
        }
      }
    });
  }

  private scrollTo(anchor: string) {
    // for any developers reading this: try not to do this, accessing the DOM directly in angular is bad practise.
    // The only reason I'm doing it here is because angular doesn't have a interface to scroll to an anchor
    // that is overflowed in a element other then the main view port.
    setTimeout(() => {
      let element = document.getElementById(anchor);
      if (element) {
        element.scrollIntoView({
          behavior: "smooth",
        });
      } else {
        console.warn("Can't find anchor", anchor);
      }
    }, 500); // Timeout is to let expansion panel animation finish before scrolling to avoid over/under shooting the anchor.
  }

  public ngAfterViewInit(): void {
    this.loaded = true;
    if (this.fragment) {
      this.scrollTo(this.fragment);
    }
  }
}
