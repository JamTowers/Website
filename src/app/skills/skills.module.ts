import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatExpansionModule } from "@angular/material/expansion";
import { MarkdownModule } from "ngx-markdown";

import { SkillsRoutingModule } from "./skills-routing.module";
import { SkillsComponent } from "./skills.component";
import { LanguagesComponent } from "./languages/languages.component";
import { ConceptsComponent } from "./concepts/concepts.component";
import { FrameworksComponent } from "./frameworks/frameworks.component";
import { AdditionalComponent } from "./additional/additional.component";
import { LanguageComponent } from "./languages/language/language.component";
import { FrameworkComponent } from "./frameworks/framework/framework.component";
import { SkillHeaderComponent } from "./skill-header/skill-header.component";

@NgModule({
  declarations: [
    SkillsComponent,
    LanguagesComponent,
    ConceptsComponent,
    FrameworksComponent,
    AdditionalComponent,
    LanguageComponent,
    FrameworkComponent,
    SkillHeaderComponent,
  ],
  imports: [
    CommonModule,
    SkillsRoutingModule,
    MarkdownModule.forChild(),
    MatProgressBarModule,
    MatExpansionModule,
  ],
})
export class SkillsModule {}
