import { Component } from "@angular/core";

import { TitleService } from "../title.service";

@Component({
  selector: "jam-skills",
  templateUrl: "./skills.component.html",
  styleUrls: ["./skills.component.scss"],
})
export class SkillsComponent {
  loading: boolean = true;

  constructor(private titleService: TitleService) {
    this.titleService.setTitle("Skills");
  }

  onLoad() {
    this.loading = false;
  }
}
