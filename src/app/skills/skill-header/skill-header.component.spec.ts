import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { SkillHeaderComponent } from "./skill-header.component";

describe("SkillHeaderComponent", () => {
  let component: SkillHeaderComponent;
  let fixture: ComponentFixture<SkillHeaderComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [SkillHeaderComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillHeaderComponent);
    component = fixture.componentInstance;
    component.skill = {
      name: "Test Skill",
      competency: 1,
      fragment: "test-skill",
    };
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
