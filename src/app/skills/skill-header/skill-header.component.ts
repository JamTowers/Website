import { Component, Input } from "@angular/core";

import { Skill } from "../skill";

@Component({
  selector: "jam-skill-header",
  templateUrl: "./skill-header.component.html",
  styleUrls: ["./skill-header.component.scss"],
})
export class SkillHeaderComponent {
  @Input() skill: Skill;

  constructor() {}
}
