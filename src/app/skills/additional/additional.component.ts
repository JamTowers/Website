import { Component } from "@angular/core";

import { TitleService } from "src/app/title.service";

@Component({
  selector: "jam-additional",
  templateUrl: "./additional.component.html",
  styleUrls: ["./additional.component.scss"],
})
export class AdditionalComponent {
  loading: boolean = true;

  constructor(private titleService: TitleService) {
    this.titleService.setTitle("Additional");
  }
}
