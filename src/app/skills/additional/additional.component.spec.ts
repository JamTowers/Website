import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { MarkdownModule } from "ngx-markdown";

import { AdditionalComponent } from "./additional.component";

describe("AdditionalComponent", () => {
  let component: AdditionalComponent;
  let fixture: ComponentFixture<AdditionalComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          HttpClientTestingModule,
          MarkdownModule.forRoot({
            loader: HttpClientTestingModule,
          }),
          MatProgressBarModule,
        ],
        declarations: [AdditionalComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
