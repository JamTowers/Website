import { Component } from "@angular/core";

import { TitleService } from "../title.service";

@Component({
  selector: "jam-about",
  templateUrl: "./about.component.html",
  styleUrls: ["./about.component.scss"],
})
export class AboutComponent {
  constructor(private titleService: TitleService) {
    this.titleService.setTitle("About");
  }
}
